<?php

namespace Drupal\Tests\static_root_page\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the condition plugins work.
 *
 * @group static_root_page
 */
class StaticRootPageTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'static_root_page',
    'locale',
    'language',
    'content_translation',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the standard profile.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);
    $this->drupalGet('/admin/config/regional/translate/settings');
    // Disable translation download.
    $this->submitForm([
      'use_source' => 'local',
    ], 'Save configuration');
    $this->drupalGet('/admin/config/regional/language/add');
    // Create FR.
    $this->submitForm([
      'predefined_langcode' => 'fr',
    ], 'Add language');
    $this->drupalGet('/admin/config/regional/language/detection/url');
    // Set prefixes to en and fr.
    $this->submitForm([
      'prefix[en]' => 'en',
      'prefix[fr]' => 'fr',
    ], 'Save configuration');
    $this->drupalGet('/admin/config/regional/language/detection');
    // Set up URL and language selection page methods.
    $this->submitForm([
      'language_interface[enabled][language-url]' => TRUE,
    ], 'Save settings');
    $this->drupalGet('/admin/structure/types/add');
    // Create content type.
    $this->submitForm([
      'name' => 'Page',
      'type' => 'page',
      'language_configuration[content_translation]' => TRUE,
    ], 'Save content type');

  }

  /**
   * Test the "language prefixes" condition with Static root page disabled.
   */
  public function testDisabledModule() {
    $this->drupalGet('/admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][static-root-page]' => FALSE,
      'language_interface[enabled][language-url]' => TRUE,
    ], 'Save settings');

    $this->drupalGet('/en');
    $this->assertSession()->elementAttributeContains('css', 'HTML', 'lang', 'en');

    $this->drupalGet('/fr');
    $this->assertSession()->elementAttributeContains('css', 'HTML', 'lang', 'fr');

    $this->drupalGet('');
    $this->assertSession()->elementAttributeContains('css', 'HTML', 'lang', 'en');
  }

  /**
   * Test the "language prefixes" condition with Static root page enabled.
   */
  public function testEnabledModule() {
    $this->drupalGet('/admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][static-root-page]' => TRUE,
      'language_interface[enabled][language-url]' => TRUE,
    ], 'Save settings');
    $this->assertSession()->checkboxChecked('language_interface[enabled][static-root-page]');

    $this->drupalGet('/en');
    $this->assertSession()->elementAttributeContains('css', 'HTML', 'lang', 'en');

    $this->drupalGet('/fr');
    $this->assertSession()->elementAttributeContains('css', 'HTML', 'lang', 'fr');

    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains('Welcome to Drupal');
    $this->assertSession()->pageTextContains('Static root page');
  }

}
