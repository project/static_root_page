<?php

/**
 * @file
 * Module code for static_root_page module.
 */

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Theme code for static_root_page module.
 */
function static_root_page_theme($existing, $type, $theme, $path) {
  $languages = [];
  foreach (\Drupal::languageManager()->getNativeLanguages() as $lang) {
    $languages[$lang->getName()] = [
      'title' => $lang->getName(),
      'id' => $lang->getId(),
      'url' => Url::fromUri('internal:/', ['language' => $lang])->toString(),
    ];
  }
  uasort($languages, [SortArray::class, 'sortByTitleElement']);
  return [
    'static_root_page' => [
      'variables' => ['language_list' => $languages],
    ],
  ];
}

/**
 * Implements hook_help().
 */
function static_root_page_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.static_root_page':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}
