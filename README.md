CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a static root page for multilingual websites with no
prefered language. No redirection for better SEO. No .htaccess changes needed.
Does not interfere with other language selection methods apart from / url.

 * For a full description of the module visit:
   https://www.drupal.org/project/static_root_page

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/static_root_page


REQUIREMENTS
------------

 * Language module


INSTALLATION
------------

Install the Static Root Page module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.

 * Provide HTML file named static-root-page.html.twig in the template folder
 of your theme
 * Go to Configuration > Languages > Detection and selection
 * Move Static Root Page to the top and enable it
 * Clear all caches


CONFIGURATION
-------------

No configuration required.


MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
